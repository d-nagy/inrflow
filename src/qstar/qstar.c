/** @mainpage
qstar topology
*/

#include <stdlib.h>
#include <stdio.h>

#include "../inrflow/misc.h"
#include "../inrflow/node.h"

long param_n; // number of dimensions

long *pow2;	// an array with the ^i, useful for doing some calculations.
long servers; // total number of servers
long switches; // total number of switches
long ports; // total number of ports

long *src_cube; // coordinates of the source server
long *dst_cube; // coordinates of the destination server
long *path; // list of ports on the optimal path
long path_index;
long diameter;

extern node_t *network;
extern routing_t routing;

static char* network_token="qstar";
static char* routing_token;
static char* topo_version="v0.1";
static char* topo_param_tokens[1]= {"n"};
//AE: make error checking so that we don't overrun this buffer
extern char filename_params[100];

/**
 * Initializes the topology and sets the dimensions
 */
long init_topo_qstar(long np, long* par)
{
    long i;

    if (np<1) {
        printf("At least 1 parameter is needed for qstar <dimensions>\n");
        exit(-1);
    }

    param_n = par[0];

    sprintf(filename_params,"n%ld",param_n);

    pow2 = malloc(param_n*sizeof(long));
    src_cube = malloc(param_n*sizeof(long));
    dst_cube = malloc(param_n*sizeof(long));

    servers = param_n;
    switches = 1;
    for (i=0; i<param_n; i++) {
        pow2[i] = switches;
        servers *= 2;
        switches *= 2;
    }

    ports = servers * 2 + switches * param_n;

    diameter = 3*switches + 2;
    path = malloc(diameter*sizeof(long));

    return 1;
}

/**
 * Release the resources used by the topology.
 */
void finish_topo_qstar()
{
    free(path);
    free(pow2);
    free(src_cube);
    free(dst_cube);
}

/**
 * Get the number of servers in the network.
 */
long get_servers_qstar()
{
    return servers;
}

/**
 * Get the number of switches in the network.
 */
long get_switches_qstar()
{
    return switches; // This is a direct topology.
}

/**
 * Get the number of ports of a given node.
 */
long get_radix_qstar(long n)
{
    if (n < servers) {
        return 2;
    }

    return param_n;
}

/**
 * Calculate connections.
 * Given a node and port, return the node and port it is connected to.
 */
tuple_t connection_qstar(long node, long port)
{
    tuple_t res;
    long sw;

    if (node < servers) {
        // If the node is a server.
        sw = node / param_n;
        long dim = node % param_n;

        if (!port) {
            // Port 0 is always connected to the nearest switch.
            res.node = servers + sw;
            res.port = dim;
        } else {
            // Port 1 is connected to a server.
            res.port = 1;
            if ((sw / pow2[dim]) % 2) {
                res.node = node - (param_n * pow2[dim]);
            } else {
                res.node = node + (param_n * pow2[dim]);
            }
        }
    } else {
        // If the node is a switch.
        res.port = 0;
        sw = node - servers;
        res.node = (sw * param_n) + port;
    }

    return res;
}

/**
 * Check whether a node is a server.
 */
long is_server_qstar(long i)
{
    return (i < servers);
}

char * get_network_token_qstar()
{
    return network_token;
}

char * get_routing_token_qstar()
{
    switch(routing) {
        case QSTAR_SS_BASIC:
            routing_token = "ss_basic";
            break;

        case QSTAR_SS_OPTIMAL:
            routing_token = "ss_optimal";
            break;

        case QSTAR_MP_OPTIMAL:
            routing_token = "mp_optimal";
            break;

        default:
            printf("Error: Unknown routing algorithm for QStar\n");
            exit(-1);
            break;
    }

    return routing_token;
}

char * get_topo_version_qstar()
{
	return topo_version;
}

char * get_topo_param_tokens_qstar(long i)
{
	return topo_param_tokens[i];
}

char * get_filename_params_qstar()
{
	return filename_params;
}

long get_server_i_qstar(long i)
{
	return i;
}

long get_switch_i_qstar(long i)
{
	return servers + i;
}

long node_to_server_qstar(long i)
{
	return i;
}

long node_to_switch_qstar(long i)
{
	return i - servers;
}

long get_ports_qstar()
{
	return ports;
}

/**
 * Get the number of paths between a source and a destination.
 * @return the number of paths.
 */
long get_n_paths_routing_qstar(long src, long dst)
{
    return 1;
}

void finish_route_qstar()
{
    path_index = 0;
};

/**
 * Simple DOR routing.
 * @return port of the current node to route packet along.
 */
long route_qstar_ss_optimal(long current, long destination)
{
    long port, curr_sw, dst_sw, dst_dim, i;
    long curr_node = current;

    #ifdef DEBUG
    if (current == destination) {
        printf("Should not be routing a packet that has arrived to its destination curr: %d, dstb: %d)!\n", current, destination);
        return -1;
    }
    #endif

    if (current < servers) {
        // If we are at a server.
        curr_sw = current / param_n;
        dst_sw = destination / param_n;

        if (curr_sw == dst_sw) {
            port = 0;
        } else {
            long curr_dim = current % param_n;
            dst_dim = destination % param_n;

            if (curr_dim == dst_dim) {
                i = (dst_dim + 1) % param_n;
            } else {
                i = curr_dim;
            }

            long out_dim;
            for (int d=0; d<param_n; d++) {
                if (!((curr_sw / pow2[i]) % 2 == (dst_sw / pow2[i]) % 2)) {
                    out_dim = i;
                    break;
                }
                i = (i + 1) % param_n;
            }

            if (out_dim == curr_dim) {
                port = 1;
            } else {
                port = 0;
            }
        }
    } else {
        // If we are at a switch.
        curr_sw = current - servers;
        dst_sw = destination / param_n;
        dst_dim = destination % param_n;

        if (curr_sw == dst_sw) {
            port = dst_dim;
        } else {
            i = (dst_dim + 1) % param_n;
            for (int d=0; d<param_n; d++) {
                if (!((curr_sw / pow2[i]) % 2 == (dst_sw / pow2[i]) % 2)) {
                    port = i;
                    break;
                }
                i = (i + 1) % param_n;
            }
        }
    }

    return (network[curr_node].port[port].faulty) ? -1 : port;
}

long route_qstar_ss_basic(long current, long destination)
{
    long port, curr_sw, dst_sw, curr_dim, dst_dim, out_dim;
    long curr_node = current;

    if (destination < servers) {
        dst_sw = destination / param_n;
        dst_dim = destination % param_n;
    } else {
        dst_sw = destination - servers;
    }

    if (current < servers) {
        curr_sw = current / param_n;

        if (curr_sw == dst_sw) {
            port = 0;
        } else {
            for (int d=0; d<param_n; d++) {
                if (curr_sw % 2 != dst_sw % 2) {
                    out_dim = d;
                    break;
                }
                curr_sw /= 2;
                dst_sw /= 2;
            }

            curr_dim = current % param_n;
            if (out_dim == curr_dim) {
                port = 1;
            } else {
                port = 0;
            }
        }
    } else {
        curr_sw = current - servers;
        if (curr_sw == dst_sw) {
            port = dst_dim;
        } else {
            for (int d=0; d<param_n; d++) {
                if (curr_sw % 2 != dst_sw % 2) {
                    port = d;
                    break;
                }
                curr_sw /= 2;
                dst_sw /= 2;
            }
        }
    }

    return (network[curr_node].port[port].faulty) ? -1 : port;
}

long route_qstar_mp_optimal(long src, long dst)
{
    long cols = 2*diameter;
    long* paths = malloc(4*cols*sizeof(long));
    long* path_lengths = calloc(4, sizeof(long));
    long* out_sw = malloc(2*sizeof(long));
    long* in_sw = malloc(2*sizeof(long));
    long* optimal_paths = malloc(4*sizeof(long));

    long curr, dst_sw, next_port, optimal, num_optimal, min_path_len = servers + switches;
    long out_1_server, in_1_server;

    out_sw[0] = network[src].port[0].neighbour.node;
    in_sw[0]= network[dst].port[0].neighbour.node;

    out_1_server = network[src].port[1].neighbour.node;
    in_1_server = network[dst].port[1].neighbour.node;

    out_sw[1]= network[out_1_server].port[0].neighbour.node;
    in_sw[1] = network[in_1_server].port[0].neighbour.node;

    // Path 0: via out_0 and in_0
    // Path 1: via out_0 and in_1
    // Path 2: via out_1 and in_0
    // Path 3: via out_1 and in_1
    // Initialise paths 0 and 1
    for (int i=0; i<2; i++) {
        paths[i*cols + path_lengths[i]++] = 0;
    }

    // Initialise paths 2 and 3
    for (int i=2; i<4; i++) {
        paths[i*cols + path_lengths[i]++] = 1;
        paths[i*cols + path_lengths[i]++] = 0;
    }

    // Calculate all paths from out switch to dst via in switch
    for (int out=0; out<2; out++) {
        for (int in=0; in<2; in++) {
            curr = out_sw[out];
            dst_sw = in_sw[in];

            // Route from out switch to in switch
            while (curr != dst_sw) {
                next_port = route_qstar_ss_basic(curr, dst_sw);
                int i = 2*out + in;
                paths[i*cols + path_lengths[i]++] = next_port;
                curr = network[curr].port[next_port].neighbour.node;
            }

            // Route from in switch to dst
            while (curr != dst) {
                next_port = route_qstar_ss_basic(curr, dst);
                int i = 2*out + in;
                paths[i*cols + path_lengths[i]++] = next_port;
                curr = network[curr].port[next_port].neighbour.node;
            }
        }
    }

    for (int i=0; i<4; i++) {
        if (path_lengths[i] < min_path_len) {
            min_path_len = path_lengths[i];
        }
    }

    num_optimal = 0;

    for (int i=0; i<4; i++) {
        if (path_lengths[i] == min_path_len) {
            optimal_paths[num_optimal++] = i;
        }
    }

    optimal = optimal_paths[rand() % num_optimal];

    for (int i=0; i<min_path_len; i++) {
        path[i] = paths[optimal*cols + i];
    }

    free(paths);
    free(path_lengths);
    free(out_sw);
    free(in_sw);
    free(optimal_paths);
    return 0;
}

long init_routing_qstar(long src, long dst)
{
    path_index = 0;

    long curr = src, next_port;

    switch (routing) {
        case QSTAR_SS_BASIC:
            while (curr != dst) {
                next_port = route_qstar_ss_basic(curr, dst);
                path[path_index++] = next_port;
                curr = network[curr].port[next_port].neighbour.node;
            }
            break;
        case QSTAR_SS_OPTIMAL:
            while (curr != dst) {
                next_port = route_qstar_ss_optimal(curr, dst);
                path[path_index++] = next_port;
                curr = network[curr].port[next_port].neighbour.node;
            }
            break;
        case QSTAR_MP_OPTIMAL:
            route_qstar_mp_optimal(src, dst);
            break;
        default:
            printf("Error: Unknown routing algorithm for Q*\n");
            exit(-1);
    }

    path_index = 0;

    return 0;
}

/**
 * @return port of the current node to route packet along.
 */
long route_qstar(long current, long destination)
{
    long port;
    long curr_node = current;

    #ifdef DEBUG
    if (current == destination) {
        printf("Should not be routing a packet that has arrived to its destination curr: %d, dstb: %d)!\n", current, destination);
        return -1;
    }
    #endif

    port = path[path_index++];

    return (network[curr_node].port[port].faulty) ? -1 : port;
}
