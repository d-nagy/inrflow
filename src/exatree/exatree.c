/** @mainpage
Euroexa tree topologies
*/
#include <stdlib.h>
#include <stdio.h>
#include "../inrflow/node.h"
#include "../inrflow/misc.h"
#include "../inrflow/globals.h"
#include "exatree.h"

long param_k;	///< parameter k of the topology, number of stages
long *param_down;	///< Number of downwards ports in each level
long *param_up;  ///< number of upwards ports in each level

long *down_pow;	///< an array with the downward-link size for different levels of the topology, useful for doing some calculations.
long *up_pow;	///< an array with the upward-link count, useful for doing some calculations.
long *sw_per_stage;  ///< an array with the number of switches per stage, used often for many purposes

long servers; 	///< The total number of servers
long switches;	///< The total number of switches
long ports;		///< The total number of links
long pbs;         ///< Number of pizza boxes (local switches)

long *cur_route;    ///< Array to store the current route
long cur_hop;   ///< Current hop in this route
long max_paths;	///< Maximum number of paths to generate when using multipath
long *path_index;	///< per-node index, used for round robin routing

static char* network_token="exatree";
static char routing_token[20];
static char* topo_version="v0.1";
static char* topo_param_tokens[11]= {"stages","down0","up0","down1","up1","down2","up2","down3","up3","down4","up4"};
//AE: make error checking so that we don't overrun this buffer
extern char filename_params[100];
static char *routing_param_tokens[1]= {"max_paths"};

/**
* Initializes the topology and sets the dimensions.
*/
long init_topo_exatree(long np, long* par)
{
	long i,j, c;
	long buffer_length;

	if (np<1) {
		printf("parameters needed\n");
		exit(-1);
	}
	param_k=par[0];
	if(param_k<1){
		printf("positive number of stages needed\n");
		exit(-1);
	}
	if(param_k>5){
		printf("number of stages limited to 5\n");
		exit(-1);
	}

	param_down=malloc(param_k*sizeof(long));
	param_up=malloc(param_k*sizeof(long));
	c=1;
	for (i=0;i<param_k;i++){
		param_down[i]=par[c++];
		param_up[i]=par[c++];
	}
    param_up[param_k-1]=0;  // up ports in the last stage should be disconnected, so let's assume they are not there, for simplicity.

	buffer_length=sprintf(filename_params,"k%ld",param_k);
	for (i=0;i<param_k;i++){
		buffer_length+=sprintf(filename_params+buffer_length,"d%ldu%ld",param_down[i],param_up[i]);
	}

	up_pow=malloc((param_k+1)*sizeof(long));
	down_pow=malloc((param_k+1)*sizeof(long));
	cur_route=malloc(2*param_k*sizeof(long));   // UP*/DOWN routes cannot be longer than 2*k
	sw_per_stage=malloc(param_k*sizeof(long));
	down_pow[0]=1;
    up_pow[0]=1;

	for (i=0; i<param_k; i++) {
		down_pow[i+1]=down_pow[i]*param_down[i];	// product of param_down[i] for 0<=i<n
		up_pow[i+1]=up_pow[i]*param_up[i];			// product of param_up[i]   for 0<=i<n
	} // numbers of up and down ports will be useful throughout,so let's compute them just once.

    switches=0;
	for (i=0; i<param_k; i++) {
		sw_per_stage[i]=up_pow[i];
		for (j=i+1;j<param_k;j++){
			sw_per_stage[i]*=param_down[j];
		}
		printf("st%ld %ld, ",i,sw_per_stage[i]);
		switches+=sw_per_stage[i];
		ports+=sw_per_stage[i]*param_down[i];
	}// number of switches per stage will be useful throughout, so let's compute them just once.
	pbs=down_pow[param_k];
	servers=16*pbs;
	switches+=pbs;
	ports+=(servers*4)+(pbs*17);

	printf("\nservers %ld\n",servers);
	printf("pbs %ld\n",pbs);
	printf("switches %ld\n",switches);

	switch(routing){
	case TREE_STATIC_ROUTING:
		snprintf(routing_token,20,"tree-static");
		break;
    case TREE_RR_ROUTING:
		snprintf(routing_token,20,"tree-roundrobin");
		path_index=malloc(servers*sizeof(long));
		for (i=0;i<servers; i++)
			path_index[i]=i;
		break;
    case TREE_RND_ROUTING:
		snprintf(routing_token,20,"tree-random");
		break;
	default:
		printf("Not a tree-compatible routing %d, switching to tree-roundrobin\n", routing);
		routing=TREE_RR_ROUTING;
		snprintf(routing_token,20,"tree-roundrobin");
		path_index=malloc(servers*sizeof(long));
		for (i=0;i<servers; i++)
			path_index[i]=i;
		break;
    }

	if(routing_nparam>0){
		max_paths=routing_params[0];
		if (max_paths<0)
			max_paths=1;
	} else {
		max_paths=1;
	}

	return 1; //Return status, not used here.
}

/**
* Release the resources used by the topology.
**/
void finish_topo_exatree()
{
    free(cur_route);
	free(up_pow);
	free(down_pow);
	free(sw_per_stage);
	free(param_down);
	free(param_up);
	if (path_index!=NULL)
		free(path_index);
}

/**
* Get the number of servers of the network
*/
long get_servers_exatree()
{
	return servers;
}

/**
* Get the number of switches of the network
*/
long get_switches_exatree()
{
	return switches;
}

// Get the number of ports of a given node (BOTH a server AND a switch, see above)
long get_radix_exatree(long node)
{
    if(node<servers)
        return 4;
    else if (node<servers+pbs) // lower part of the local switch;
        return 17;
    else    // higher part of the local switch and rest of the network
    {
        long k=node-servers-pbs;
        long i=0;

        while (k>=sw_per_stage[i])
        {
            k-=sw_per_stage[i];
            i++;
        }
        //printf("%ld (st %ld) = %ld\n",node,i,param_down[i]+param_up[i]);
        return param_down[i]+param_up[i];
    }

}

/**
* Calculates connections
*/
tuple_t connection_exatree(long node, long port)
{
	tuple_t res;
	long lvl;
	long pos;
	long nl_first; // id of the first switch in the NEIGHBOURING level
	long local_sw;

	if(node<servers)
    {
        if (port==0)// uplink to the local switch
        {
            res.node=servers + (node/16);
            res.port=node%16;
        }
        else // local 4-node arrangement
        {
            res.node=((node/4)*4) + (node+port)%4;
            res.port=4-port;
        }
    }
    else if (node<servers+pbs) // lower part of the local switch;
    {
        local_sw=node-servers;
        if (port < 16)
        {
            res.node=(local_sw*16)+port;
            res.port=0;
        }
        if (port==16)
        {
            res.node=node+pbs;
            res.port=0;
        }
    }
    else    // higher part of the local switch and network
    {
        {
            pos=node-servers-pbs;
            lvl=0;
            nl_first=servers+pbs;
            while (pos>=sw_per_stage[lvl]){
                pos-=sw_per_stage[lvl];
                nl_first+=sw_per_stage[lvl];
                lvl++;
            }
            if (lvl==param_k-1 && port>=param_down[lvl]) {   //disconnected links in the last stage of the tree (param_up[last] should be 0 any way!!!)
                res.node=-1;
                res.port=-1;
            } else if (lvl==0 && port<param_down[lvl] ) { // connected to lower level of switches
                res.node=servers+pos;
                res.port=16;
            } else if (port>=param_down[lvl]) { //upwards port
                long p = port-param_down[lvl];
                nl_first+= sw_per_stage[lvl]; // we are connecting up, so the neighbouring level is above, we need to add the number of switches in this level
                res.node = (p*up_pow[lvl]) + (mod(pos,up_pow[lvl])) + ((pos/(param_down[lvl+1]*up_pow[lvl]))*up_pow[lvl+1]) + nl_first;
                res.port = (mod(pos/up_pow[lvl],param_down[lvl+1]));
            } else { //downwards port
                nl_first-=sw_per_stage[lvl-1]; // we are connecting down, so the neighbouring level is below, we need to substract the number of switches in the previous level
                res.node = (port*up_pow[lvl-1]) + (mod(pos,up_pow[lvl-1])) + ((pos/up_pow[lvl])*up_pow[lvl-1]*param_down[lvl])  + nl_first;
                res.port = param_down[lvl-1] + mod(pos/up_pow[lvl-1], param_up[lvl-1]);
            }
        }
    }

	return res;
}

long is_server_exatree(long i)
{
	return (i<servers);
}


char * get_network_token_exatree()
{
	return network_token;
}

char * get_routing_token_exatree()
{
	return routing_token;
}

char *get_routing_param_tokens_exatree(long i){

    return routing_param_tokens[i];
}

char * get_topo_version_exatree()
{
	return topo_version;
}

char * get_topo_param_tokens_exatree(long i)
{
	return topo_param_tokens[i];
}

char * get_filename_params_exatree()
{
	return filename_params;
}

long get_server_i_exatree(long i)
{
	return i;
}

long get_switch_i_exatree(long i)
{
	return i+servers;
}

long node_to_server_exatree(long i)
{
	return i;
}

long node_to_switch_exatree(long i)
{
	return i-servers;
}

long get_ports_exatree()
{
	return ports;
}

/**
* Get the number of paths between a source and a destination.
* @return the number of paths.
*/
long get_n_paths_routing_exatree(long s, long d){

	return (1);

    long src=s/16;
    long dst=d/16;
    long mca=0;

	switch(routing){
	case TREE_STATIC_ROUTING:
		return (1);
		break;
    case TREE_RND_ROUTING:
    case TREE_RR_ROUTING:
    	while (src/down_pow[mca]!=dst/down_pow[mca]) {
			mca++;
		}
    	return max(min(up_pow[mca],max_paths),1);
		break;
	default:
		printf("Not a tree-compatible routing %d", routing);
		exit(-1);
	}
}


/**
* Simple oblivious UP/DOWN. Others can be implemented.
*/
long init_routing_exatree_static(long s, long d){

    long src=s/16;
    long dst=d/16;
	long mca=0; // minimum common ancestor (levels)
	long i;

	while (src/down_pow[mca]!=dst/down_pow[mca]) {
		mca++;
	}

	cur_route[0]=0; //first hop is away from server, so no option to choose.
	for (i=1; i<mca; i++) { // Choose option based on source server, ensures load balancing.
		cur_route[i]=param_down[i-1]+((src/down_pow[i-1]) % param_up[i-1]);
	}

	for (i=0; i<mca; i++) {
		cur_route[mca+i]=(dst/down_pow[mca-1-i]) % param_down[mca-1-i];
	}

	cur_hop=1;
	return 0;
}

/**
* Simple randomized UP/DOWN. Multipath-capable
*/
long init_routing_exatree_random(long s, long d){

    long src=s/16;
    long dst=d/16;
    long mca=0;
	long i;
	long p=rand();	// a random number used to select the path to use

	while (src/down_pow[mca]!=dst/down_pow[mca]) {
		mca++;
	}

	cur_route[0]=0; //first hop is away from server, so no option to choose.
	for (i=1; i<mca; i++) { // Choose option based on source server, ensures load balancing.
		cur_route[i]=param_down[i-1]+((p/down_pow[i-1]) % param_up[i-1]);
	}

	for (i=0; i<mca; i++) {
		cur_route[mca+i]=(dst/down_pow[mca-1-i]) % param_down[mca-1-i];
	}

	cur_hop=1;
	return 0;
}

/**
* Simple UP/DOWN with round robin between all possible paths. Multipath-capable
*/
long init_routing_exatree_roundrobin(long s, long d){

    long src=s/16;
    long dst=d/16;
    long mca=0;
	long i;

	while (src/down_pow[mca]!=dst/down_pow[mca]) {
		mca++;
	}

	cur_route[0]=0; //first hop is away from server, so no option to choose.
	for (i=1; i<mca; i++) { // Choose option based on source server, ensures load balancing.
		cur_route[i]=param_down[i-1]+((path_index[src]/down_pow[i-1]) % param_up[i-1]);
	}

	for (i=0; i<mca; i++) {
		cur_route[mca+i]=(dst/down_pow[mca-1-i]) % param_down[mca-1-i];
	}
	path_index[src]=(path_index[src]+1)%servers;
	cur_hop=1;
	return 0;
}


/**
* Routing selector. Others can be implemented.
*/
long init_routing_exatree(long src, long dst)
{
	switch(routing){
	case TREE_STATIC_ROUTING:
		return init_routing_exatree_static(src, dst);
		break;
    case TREE_RND_ROUTING:
		return init_routing_exatree_random(src, dst);
		break;
    case TREE_RR_ROUTING:
    	return init_routing_exatree_roundrobin(src, dst);
		break;
	default:
		printf("Not a tree-compatible routing %d", routing);
		exit(-1);
	}
}

void finish_route_exatree()
{

}

/**
* Simple oblivious DOR.
*/
long route_exatree(long current, long destination)
{
	long c;

	//printf ("%ld ",current);

#ifdef DEBUG
	if(current==destination) {
		printf("should not be routing a packet that has arrived to its destination (curr: %ld, dstb: %ld)!\n", current, destination);
		return -1; // just in case, let's abort the routing
	}
#endif // DEBUG

    if ((current/4)==(destination/4)) // in the same 4-node group
    {
        c=(destination%4)-(current%4);
        if (c<0)
            c+=4;
        return c;
    }
    else if (current<servers)
    {
        return 0; // uplink to local switch
    }
    else if ((current-servers)==(destination/16))
    {
        return destination%16; // downlink to destination
    }
    else if (current<servers+pbs)
    {
        return 16; // uplink to upper switch
    }
    else
    {
        return cur_route[cur_hop++];
    }
}

