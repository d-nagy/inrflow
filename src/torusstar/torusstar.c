/** @mainpage
torusstar topology
*/

#include <stdlib.h>
#include <stdio.h>

#include "../inrflow/misc.h"
#include "../inrflow/node.h"

long param_n; // number of dimensions
long* nodes_dim; // number of nodes per dimension
long* steps; // node intervals per dimension

long servers; // total number of servers
long switches; // total number of switches
long ports; // total number of ports

long *src_tstar; // coordinates of the source server
long *dst_tstar; // coordinates of the destination server
long *path; // list of ports on the optimal path
long path_index;
long diameter;

extern node_t *network;
extern routing_t routing;

static char* network_token="torusstar";
static char* routing_token;
static char* topo_version="v0.1";
static char* topo_param_tokens[11]= {"X","Y","Z","A","B","C","D","E","F","G","H"};
//AE: make error checking so that we don't overrun this buffer
extern char filename_params[100];

/**
 * Initializes the topology and sets the dimensions
 */
long init_topo_torusstar(long np, long* par)
{
    long i, k = 0, step = 1;

    if (np<1) {
        printf("At least 1 parameter is needed for torusstar <dimensions>\n");
        exit(-1);
    }

    param_n = np;
    nodes_dim = malloc(param_n*sizeof(long));
    steps = malloc(param_n*sizeof(long));

    src_tstar = malloc(param_n*sizeof(long));
    dst_tstar = malloc(param_n*sizeof(long));

    switches = 1;

    for (i=0; i<param_n; i++) {
        steps[i] = step;
        nodes_dim[i] = par[i];
        diameter += nodes_dim[i] / 2;
        switches *= par[i];
        k += sprintf(filename_params+k, "%s%ld", topo_param_tokens[i], nodes_dim[i]);
        step *= nodes_dim[i];
    }

    diameter = diameter*3 + 2;
    path = malloc(diameter*sizeof(long));

    servers = param_n * switches * 2;
    ports = (servers * 2) + (switches * param_n * 2);

    return 1;
}

/**
 * Release the resources used by the topology.
 */
void finish_topo_torusstar()
{
    free(path);
    free(src_tstar);
    free(dst_tstar);
    free(nodes_dim);
    free(steps);
}

/**
 * Get the number of servers in the network.
 */
long get_servers_torusstar()
{
    return servers;
}

/**
 * Get the number of switches in the network.
 */
long get_switches_torusstar()
{
    return switches; // This is a direct topology.
}

/**
 * Get the number of ports of a given node.
 */
long get_radix_torusstar(long n)
{
    if (n < servers) {
        return 2;
    } else {
        return 2*param_n;
    }
}

/**
 * Calculate connections.
 * Given a node and port, return the node and port it is connected to.
 */
tuple_t connection_torusstar(long node, long port)
{
    tuple_t res;
    long sw;

    if (node < servers) {
        // If node is a server
        sw = node / (2 * param_n);
        long orientation = node % (2 * param_n);

        if (!port) {
            // Port 0 is towards a switch
            res.node = servers + sw;
            res.port = orientation;
        } else {
            // Port 1 is towards a server
            res.port = 1;
            long dim = orientation / 2;
            long dir = (orientation % 2) ? -1 : 1;

            for (int i=0; i<param_n; i++) {
                int d = nodes_dim[i];
                src_tstar[i] = sw % d;
                sw /= d;
            }

            long new_coord = (src_tstar[dim] + dir) % nodes_dim[dim];
            if (new_coord < 0) {
                new_coord += nodes_dim[dim];
            }

            src_tstar[dim] = new_coord;

            long t_sw = 0;
            for (int i=0; i<param_n; i++) {
                t_sw += steps[i] * src_tstar[i];
            }

            res.node = 2 * param_n * t_sw + (orientation + dir);
        }
    } else {
        // If node is a switch
        res.port = 0;
        sw = node - servers;
        res.node = 2 * param_n * sw + port;
    }

    return res;
}

/**
 * Check whether a node is a server.
 */
long is_server_torusstar(long i)
{
    return (i < servers);
}

char * get_network_token_torusstar()
{
    return network_token;
}

char * get_routing_token_torusstar()
{
    switch(routing) {
        case TORUSSTAR_BASIC:
            routing_token = "basic";
            break;
        case TORUSSTAR_OPTIMAL:
            routing_token = "optimal";
            break;
        default:
            printf("Error: Unknown routing algorithm for TorusStar\n");
            exit(-1);
            break;
    }

    return routing_token;
}

char * get_topo_version_torusstar()
{
	return topo_version;
}

char * get_topo_param_tokens_torusstar(long i)
{
	return topo_param_tokens[i];
}

char * get_filename_params_torusstar()
{
	return filename_params;
}

long get_server_i_torusstar(long i)
{
	return i;
}

long get_switch_i_torusstar(long i)
{
	return servers + i;
}

long node_to_server_torusstar(long i)
{
	return i;
}

long node_to_switch_torusstar(long i)
{
	return i - servers;
}

long get_ports_torusstar()
{
	return ports;
}

/**
 * Get the number of paths between a source and a destination.
 * @return the number of paths.
 */
long get_n_paths_routing_torusstar(long src, long dst)
{
    return 1;
}

void finish_route_torusstar()
{
    path_index = 0;
};

long route_tstar_basic(long current, long destination)
{
    long port, curr_sw, dst_sw, dst_orientation, dst_dim;

    int i, step, d, diff;

    if (destination < servers) {
        dst_sw = destination / (2 * param_n);
        dst_orientation = destination % (2 * param_n);
        dst_dim = dst_orientation / 2;
    } else {
        dst_sw = destination - servers;
        dst_orientation = -1;
        dst_dim = -1;
    }


    if (current < servers) {
        // If we are at a server
        curr_sw = current / (2 * param_n);

        if (curr_sw == dst_sw) {
            port = 0;
        } else {
            long arcs_p, curr_dir, dim_arcs, out_dim = 0;
            long curr_orientation = current % (2 * param_n);
            long curr_dim = curr_orientation / 2;

            i = curr_dim;

            for (int k=0; k<param_n; k++) {
                step = steps[i];
                d = nodes_dim[i];
                diff = ((dst_sw / step) % d) - ((curr_sw / step) % d);

                if (diff != 0) {
                    out_dim = i;

                    if (out_dim == curr_dim) {
                        arcs_p = diff;
                        if (diff < 0) {
                            arcs_p += d;
                        }
                        arcs_p *= 3;

                        curr_dir = curr_orientation % 2;
                        arcs_p += (curr_dir) ? 1 : -1;

                        dim_arcs = 3 * d;

                        if (2 * arcs_p < dim_arcs && !curr_dir) {
                            port = 1;
                        } else if (2 * arcs_p >= dim_arcs && curr_dir) {
                            port = 1;
                        } else {
                            port = 0;
                        }
                    } else {
                        port = 0;
                    }

                    break;
                }

                i = (i + 1) % param_n;
            }
        }
    } else {
        // If we are at a switch
        curr_sw = current - servers;

        if (curr_sw == dst_sw) {
            port = dst_orientation;
        } else {
            i = (dst_dim + 1) % param_n;

            for (int k=0; k<param_n; k++) {
                step = steps[i];
                d = nodes_dim[i];
                diff = ((dst_sw / step) % d) - ((curr_sw / step) % d);

                if (2 * diff > d) {
                    // Travelling in -ve direction
                    port = (2 * i) + 1;
                    break;
                } else if (2 * diff < -d) {
                    // Travelling in +ve direction
                    port = 2 * i;
                    break;
                } else if (2 * abs(diff) == d) {
                    if (i == dst_dim) {
                        port = (2 * i) + ((dst_orientation + 1) % 2);
                        break;
                    } else {
                        port = (2 * i) + (rand() % 2);
                        break;
                    }
                } else if (diff != 0) {
                    if (diff > 0) {
                        // Travelling in +ve direction
                        port = 2 * i;
                        break;
                    } else {
                        // Travelling in -ve direction
                        port = (2 * i) + 1;
                        break;
                    }
                }

                i = (i + 1) % param_n;
            }
        }
    }

    return port;
}

long route_tstar_optimal(long src, long dst)
{
    long cols = 2*diameter;
    long* paths = malloc(4*cols*sizeof(long));
    long* path_lengths = calloc(4, sizeof(long));
    long* out_sw = malloc(2*sizeof(long));
    long* in_sw = malloc(2*sizeof(long));
    long* optimal_paths = malloc(4*sizeof(long));

    long curr, dst_sw, next_port, optimal, num_optimal, min_path_len = servers + switches;
    long out_1_server, in_1_server;

    out_sw[0] = network[src].port[0].neighbour.node;
    in_sw[0]= network[dst].port[0].neighbour.node;

    out_1_server = network[src].port[1].neighbour.node;
    in_1_server = network[dst].port[1].neighbour.node;

    out_sw[1]= network[out_1_server].port[0].neighbour.node;
    in_sw[1] = network[in_1_server].port[0].neighbour.node;

    // Path 0: via out_0 and in_0
    // Path 1: via out_0 and in_1
    // Path 2: via out_1 and in_0
    // Path 3: via out_1 and in_1
    // Initialise paths 0 and 1
    for (int i=0; i<2; i++) {
        paths[i*cols + path_lengths[i]++] = 0;
    }

    // Initialise paths 2 and 3
    for (int i=2; i<4; i++) {
        paths[i*cols + path_lengths[i]++] = 1;
        paths[i*cols + path_lengths[i]++] = 0;
    }

    // Calculate all paths from out switch to dst via in switch
    for (int out=0; out<2; out++) {
        for (int in=0; in<2; in++) {
            curr = out_sw[out];
            dst_sw = in_sw[in];

            // Route from out switch to in switch
            while (curr != dst_sw) {
                next_port = route_tstar_basic(curr, dst_sw);
                int i = 2*out + in;
                paths[i*cols + path_lengths[i]++] = next_port;
                curr = network[curr].port[next_port].neighbour.node;
            }

            // Route from in switch to dst
            while (curr != dst) {
                next_port = route_tstar_basic(curr, dst);
                int i = 2*out + in;
                paths[i*cols + path_lengths[i]++] = next_port;
                curr = network[curr].port[next_port].neighbour.node;
            }
        }
    }

    for (int i=0; i<4; i++) {
        if (path_lengths[i] < min_path_len) {
            min_path_len = path_lengths[i];
        }
    }

    num_optimal = 0;

    for (int i=0; i<4; i++) {
        if (path_lengths[i] == min_path_len) {
            optimal_paths[num_optimal++] = i;
        }
    }

    optimal = optimal_paths[rand() % num_optimal];

    for (int i=0; i<min_path_len; i++) {
        path[i] = paths[optimal*cols + i];
    }

    free(paths);
    free(path_lengths);
    free(out_sw);
    free(in_sw);
    free(optimal_paths);
    return 0;
}

long init_routing_torusstar(long src, long dst)
{
    path_index = 0;

    long curr = src, next_port;

    switch (routing) {
        case TORUSSTAR_BASIC:
            while (curr != dst) {
                next_port = route_tstar_basic(curr, dst);
                path[path_index++] = next_port;
                curr = network[curr].port[next_port].neighbour.node;
            }
            break;
        case TORUSSTAR_OPTIMAL:
            route_tstar_optimal(src, dst);
            break;
        default:
            printf("Error: Unknown routing algorithm for TorusStar\n");
            exit(-1);
    }

    path_index = 0;

    return 0;
}

/**
 * @return port of the current node to route packet along.
 */
long route_torusstar(long current, long destination)
{
    long port;
    long curr_node = current;

    #ifdef DEBUG
    if (current == destination) {
        printf("Should not be routing a packet that has arrived to its destination curr: %d, dstb: %d)!\n", current, destination);
        return -1;
    }
    #endif

    port = path[path_index++];

    return (network[curr_node].port[port].faulty) ? -1 : port;
}
