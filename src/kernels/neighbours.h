#ifndef _neighbours
#define _neighbours

void mesh2d_woc(application *app);

void mesh2d_wc(application *app);

void mesh3d_woc(application *app);

void mesh3d_wc(application *app);

void torus2d_woc(application *app);

void torus3d_woc(application *app);

void torus2d_wc(application *app);

void torus3d_wc(application *app);

void waterfall(application *app);


#endif
