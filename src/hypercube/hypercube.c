/** @mainpage
hypercube topology
*/

#include <stdlib.h>
#include <stdio.h>

#include "../inrflow/node.h"

long param_n; // number of dimensions

long *n_pow;	// an array with the n^i, useful for doing some calculations.
long servers; // total number of servers
long ports; // total number of ports

long *src_cube; // coordinates of the source server
long *dst_cube; // coordinates of the destination server

extern node_t *network;
static char* network_token="hypercube";
static char* routing_token="dimensional";
static char* topo_version="v0.1";
static char* topo_param_tokens[1]= {"n"};
//AE: make error checking so that we don't overrun this buffer
extern char filename_params[100];

/**
 * Initializes the topology and sets the dimensions
 */
long init_topo_hypercube(long np, long* par)
{
    long i;

    if (np<1) {
        printf("At least 1 parameter is needed for hypercube <dimensions>\n");
        exit(-1);
    }

    param_n = par[0];

    sprintf(filename_params,"n%ld",param_n);

    n_pow = malloc(param_n*sizeof(long));
    src_cube = malloc(param_n*sizeof(long));
    dst_cube = malloc(param_n*sizeof(long));

    servers = 1;
    for (i=0; i<param_n; i++) {
        n_pow[i] = servers;
        servers *= 2;
    }

    ports = servers * param_n;

    return 1;
}

/**
 * Release the resources used by the topology.
 */
void finish_topo_hypercube()
{
    free(n_pow);
    free(src_cube);
    free(dst_cube);
}

/**
 * Get the number of servers in the network.
 */
long get_servers_hypercube()
{
    return servers;
}

/**
 * Get the number of switches in the network.
 */
long get_switches_hypercube()
{
    return 0; // This is a direct topology.
}

/**
 * Get the number of ports of a given node.
 */
long get_radix_hypercube(long n)
{
    return param_n;
}

/**
 * Calculate connections.
 * Given a node and port, return the node and port it is connected to.
 */
tuple_t connection_hypercube(long node, long port)
{
    tuple_t res;
    long x = n_pow[port];

    if ((node / x) % 2) {
        res.node = node - n_pow[port];
    } else {
        res.node = node + n_pow[port];
    }

    res.port = port;

    return res;
}

/**
 * Check whether a node is a server.
 */
long is_server_hypercube(long i)
{
    return (i < servers);
}

char * get_network_token_hypercube()
{
    return network_token;
}

char * get_routing_token_hypercube()
{
    return routing_token;
}

char * get_topo_version_hypercube()
{
	return topo_version;
}

char * get_topo_param_tokens_hypercube(long i)
{
	return topo_param_tokens[i];
}

char * get_filename_params_hypercube()
{
	return filename_params;
}

long get_server_i_hypercube(long i)
{
	return i;
}

long get_switch_i_hypercube(long i)
{
	return i;
}

long node_to_server_hypercube(long i)
{
	return i;
}

long node_to_switch_hypercube(long i)
{
	return i;
}

long get_ports_hypercube()
{
	return ports;
}

/**
 * Get the number of paths between a source and a destination.
 * @return the number of paths.
 */
long get_n_paths_routing_hypercube(long src, long dst)
{
    return 1;
}

long init_routing_hypercube(long s, long d)
{
    return 0;
}

void finish_route_hypercube() { };

/**
 * Simple DOR routing.
 * @return port of the current node to route packet along.
 */
long route_hypercube(long current, long destination)
{
    long port;
    long curr_node = current;

    #ifdef DEBUG
    if (current == destination) {
        printf("Should not be routing a packet that has arrived to its destination curr: %d, dstb: %d)!\n", current, destination);
        return -1;
    }
    #endif

    for (port=0; port<param_n; port++) {
        if (current % 2 == destination % 2) {
            current /= 2;
            destination /= 2;
        } else {
            break;
        }
    }

    return (network[curr_node].port[port].faulty) ? -1 : port;
}
