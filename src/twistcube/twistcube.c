/** @mainpage
twisted cube topology
*/

#include <stdlib.h>
#include <stdio.h>

#include "../inrflow/node.h"

long param_n; // number of dimensions
long param_m; // m such that 2m + 1 = n

long *n_pow;	// an array with the n^i, useful for doing some calculations.
long servers; // total number of servers
long ports; // total number of ports

long *src_cube; // coordinates of the source server
long *dst_cube; // coordinates of the destination server

extern node_t *network;
static char* network_token="twistcube";
static char* routing_token="dimensional";
static char* topo_version="v0.1";
static char* topo_param_tokens[1]= {"n"};
//AE: make error checking so that we don't overrun this buffer
extern char filename_params[100];

/*
 * Calculate parity of an array of bits
 */
int parity_tq(long* bits, int lower, int upper)
{
    long sum = 0;
    for (int i=lower; i<upper; i++) {
        sum += bits[i];
    }

    return sum % 2;
}

/**
 * Initializes the topology and sets the dimensions
 */
long init_topo_twistcube(long np, long* par)
{
    long i;

    if (np<1) {
        printf("At least 1 parameter is needed for twistcube <dimensions>\n");
        exit(-1);
    }

    param_n = par[0];

    if (param_n % 2 == 0) {
        printf("Dimension must be odd for twistcube topology\n");
        exit(-1);
    }

    param_m = (param_n - 1) / 2;

    sprintf(filename_params,"n%ld",param_n);

    n_pow = malloc(param_n*sizeof(long));
    src_cube = malloc(param_n*sizeof(long));
    dst_cube = malloc(param_n*sizeof(long));

    servers = 1;
    for (i=0; i<param_n; i++) {
        n_pow[i] = servers;
        servers *= 2;
    }

    ports = servers * param_n;

    return 1;
}

/**
 * Release the resources used by the topology.
 */
void finish_topo_twistcube()
{
    free(n_pow);
    free(src_cube);
    free(dst_cube);
}

/**
 * Get the number of servers in the network.
 */
long get_servers_twistcube()
{
    return servers;
}

/**
 * Get the number of switches in the network.
 */
long get_switches_twistcube()
{
    return 0; // This is a direct topology.
}

/**
 * Get the number of ports of a given node.
 */
long get_radix_twistcube(long n)
{
    return param_n;
}

/**
 * Calculate connections.
 * Given a node and port, return the node and port it is connected to.
 */
tuple_t connection_twistcube(long node, long port)
{
    tuple_t res;
    long x = n_pow[port];

    if (port % 2 == 0) {
        // Cannot be a twisted edge on even dimensions
        if ((node / x) % 2) {
            res.node = node - x;
        } else {
            res.node = node + x;
        }
    } else {
        int j = (port + 1) / 2;

        int temp = node;
        for (int i=0; i<param_n; i++) {
            src_cube[i] = temp % 2;
            temp /= 2;
        }

        if (parity_tq(src_cube, 0, 2*j-1) == 0) {
            // We have a twisted link
            src_cube[2*j] = (src_cube[2*j] + 1) % 2;
            src_cube[2*j-1] = (src_cube[2*j-1] + 1) % 2;

            res.node = 0;
            for (int i=0; i<param_n; i++) {
                res.node += src_cube[i] * n_pow[i];
            }
        } else {
            // Normal edge
            if ((node / x) % 2) {
                res.node = node - x;
            } else {
                res.node = node + x;
            }
        }
    }

    res.port = port;

    return res;
}

/**
 * Check whether a node is a server.
 */
long is_server_twistcube(long i)
{
    return (i < servers);
}

char * get_network_token_twistcube()
{
    return network_token;
}

char * get_routing_token_twistcube()
{
    return routing_token;
}

char * get_topo_version_twistcube()
{
	return topo_version;
}

char * get_topo_param_tokens_twistcube(long i)
{
	return topo_param_tokens[i];
}

char * get_filename_params_twistcube()
{
	return filename_params;
}

long get_server_i_twistcube(long i)
{
	return i;
}

long get_switch_i_twistcube(long i)
{
	return i;
}

long node_to_server_twistcube(long i)
{
	return i;
}

long node_to_switch_twistcube(long i)
{
	return i;
}

long get_ports_twistcube()
{
	return ports;
}

/**
 * Get the number of paths between a source and a destination.
 * @return the number of paths.
 */
long get_n_paths_routing_twistcube(long src, long dst)
{
    return 1;
}

long init_routing_twistcube(long s, long d)
{
    return 0;
}

void finish_route_twistcube() { };

/**
 * Simple DOR routing.
 * @return port of the current node to route packet along.
 */
long route_twistcube(long current, long destination)
{
    long port = -1, temp_curr, temp_dst, j;
    long curr_node = current;

    int bit0_eq, bit1_eq;

    #ifdef DEBUG
    if (current == destination) {
        printf("Should not be routing a packet that has arrived to its destination curr: %d, dstb: %d)!\n", current, destination);
        return -1;
    }
    #endif

    temp_curr = current;
    temp_dst = destination;

    for (int i=0; i<param_n; i++) {
        src_cube[i] = temp_curr % 2;
        dst_cube[i] = temp_dst % 2;
        temp_curr /= 2;
        temp_dst /= 2;
    }

    // Find leftmost differing double bit j that can be corrected by a single link
    for (j=param_m; j>-1; j--) {
        bit0_eq = src_cube[2*j-1] == dst_cube[2*j-1];
        bit1_eq = src_cube[2*j] == dst_cube[2*j];

        if (!bit0_eq || !bit1_eq) {
            // Found leftmost differing double bit j

            // Now check if double bit differs in only one bit
            if (bit0_eq) {
                port = 2*j;
                break;
            } else if (bit1_eq) {
                if (parity_tq(src_cube, 0, 2*j-1) == 1) {
                    port = 2*j - 1;
                    break;
                } else {
                    continue;
                }
            }

            // Check if we can correct the entire double bit with a twisted link
            if (parity_tq(src_cube, 0, 2*j-1) == 0) {
                port = 2*j - 1;
                break;
            }
        }
    }

    // Check the 0th double bit (the least significant bit)
    if (port < 0 && current % 2 != destination % 2) {
        port = 0;
    }
    // Find the rightmost differing double bit j and correct the first bit of it
    else if (port < 0) {
        for (int j=1; j<=param_m; j++) {
            bit0_eq = src_cube[2*j-1] == dst_cube[2*j-1];
            bit1_eq = src_cube[2*j] == dst_cube[2*j];

            if (!bit0_eq || !bit1_eq) {
                // Found rightmost differing double bit j
                port = 2*j;
                break;
            }
        }
    }

    return (network[curr_node].port[port].faulty) ? -1 : port;
}
