#ifndef _io
#define _io

long load_mapping_from_file(char *file, long ntasks, long *translation);

long generate_bfs_file();
#endif
